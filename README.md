# Multiple VMs parallel provisioning with Ansible Only

The main idea of this project was to deliver Ansible only parallel machine provisioning playbook on RHV and VMware.
Main steps:

* Service creation with Embeded Ansible
* Multiple VM provisioning in parallel on RHV
* Bounding newly created VMs to the service


## RHV Multi VM Parallel Provisioning

To successfully create this task on RHV we use ``` ovirt_vm ``` ansible module.

Below you can find the documentation link:

https://docs.ansible.com/ansible/latest/modules/ovirt_vm_module.html

Here you have an example alredy implemented as part of our CloudForms Ansible aotomation:

```
#    - name: Obtain SSO token
#      ovirt_auth:
#        username: "{{ rhv_username }}"
#        password: "{{ rhv_password }}"
#        url: "{{ rhv_hostname }}"
#        insecure: yes

    - name: Create VM web servers
      ovirt_vm:
#        auth: "{{ ovirt_auth }}"
        name: "{{ item }}"
        cluster: "{{ cluster_name }}"
        operating_system: rhel_7x64
        template: "{{ rhv_template }}"
        state: running
        type: server
      register: newvm
```

Please pay attention to the commented lines. You normally need it when you start Ansible playbook from Ansible Engine (Core).
As we have part of Ansible Tower implemented in CloudForms we can utilize credentials funcionality.
This option will automatically create SSO token for RHV.


**But how to make it parallel?**

For that we used ``` async ``` mode in Ansible which allowes us to start parallel vm provisioning

```
    - name: Create VM web servers
      ovirt_vm:
#        auth: "{{ ovirt_auth }}"
        name: "{{ item }}"
        cluster: "{{ cluster_name }}"
        operating_system: rhel_7x64
        template: "{{ rhv_template }}"
        state: running
        type: server
      register: newvm
      async: '50'
      poll: '0'
      with_items: "{{ vm_name }}"

```

One thing is ``` async ``` mode and the other thing is checking the job status.
We have an example for that:

```
      
    - name: 'Check status'
      async_status: jid={{ item.ansible_job_id }}
      register: 'job_result'
      until: 'job_result.finished'
      retries: 100
      delay: 5
      with_items: '{{ newvm.results }}'

```

This is basically it for Ansible to work.

After that part we can observe sth like that:

![](pictures/empty-service.png)

**Where are my VMs!!!!**

We need to link it to the service.

To do that there is a need to play a little bit with CloudForms API

First of all we need to refresh RHV provider in CloudForms:

```
   - name: Refresh RHV provider in CloudForms
      uri:
        url: "{{ provider_url }}"
        method: POST
        body:
          action: refresh
        body_format: json
        validate_certs: False
        headers:
          X-Auth-Token: "{{ manageiq.api_token }}"
          Content-Type: "application/json"
        status_code: 200
      register: output

```

and wait for provider to refresh:

```
    - name: Wait for the provider refresh to end
      uri:
        url: "{{ task_url }}"
        method: GET
        validate_certs: False
        headers:
          X-Auth-Token: "{{ manageiq.api_token }}"
          Content-Type: "application/json"
        status_code: 200
      register: task_result
      until: task_result.json.state == 'Finished' or task_result.json.status == 'Error'
      failed_when: task_result.json.status == 'Error'
      retries: "{{max_retries}}"
      delay: "{{retry_interval}}"

```

Then create API urls of all VMs we created and put it into a list:

```
    - name: Lookup instances href
      uri:
        url: "{{ manageiq.api_url }}/api/vms?filter[]=name={{ item }}&expand=resources"
        method: GET
        body:
          action: refresh
        body_format: json
        validate_certs: False
        headers:
          X-Auth-Token: "{{ manageiq.api_token }}"
          Content-Type: "application/json"
        status_code: 200
      register: output
      with_items: "{{ vm_name }}"

    - name: Set the Service URL
      set_fact:
        svc_url: "/api/{{ manageiq.service }}"

    - name: Initialize an empty list for vms
      set_fact:
        vms: []

    - name: Append resource href to vms list
      set_fact:
        vms: "{{ vms }} + [ { 'href': svc_url, 'resource': { 'href': '/api/vms/{{ item.json.resources[0].id }}' } } ]"
      with_items: "{{ output.results }}"

```

Finally we can sync previously created VMs with service:

```
    - name: Register vms with the service
      uri:
        url: "{{ manageiq.api_url }}/api/services"
        method: POST
        body_format: json
        body:
          action: add_resource
          resources: "{{ vms }}"
        validate_certs: False
        headers:
          X-Auth-Token: "{{ manageiq.api_token }}"
          Content-Type: "application/json"
        status_code: 200
      register: output

```

After all we can see fully populated service:

![](pictures/)

**CloudForms API** |
---|
To make it work in multi appliance scenario we need to tune advanced options on each appliance |
from ``` :session_store: cache ```  to  ``` :session_store: sql ``` |
